import axios from 'axios'
import { createStore } from 'vuex'

export default createStore({
  state: {
    full_city: null,
    city: [],
    casetype: [],
  },
  getters: {
    full_city(state){
      return state.full_city;
    },
    city(state){
      return state.city;
    },
    casetype(state){
      return state.casetype;
    }
  },
  mutations: {
    get_city(state, payload){
      state.full_city = payload;
      payload.forEach(element => {
        state.city.push(element.city);
      })
    },
    get_casetype(state, payload){
      state.casetype = payload;
    }
  },
  actions: {
    // 取得城市和鄉鎮市
    get_city({commit}){
      axios.get("http://house-api.tonyyen.org/api/area")
      .then((res) =>{
        commit('get_city', res.data);
      })
    },
    // 取得城市房屋型態
    get_casetype({commit}, {newcity}){
      axios.get("http://house-api.tonyyen.org/api/casetype",{params:{city: newcity}})
      .then((res) =>{
        commit('get_casetype', res.data);
      })
    }
  }
})
