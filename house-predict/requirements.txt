numpy == 1.19.5
pymysql == 1.0.2
python-dateutil == 2.8.1
joblib == 0.16.0
sklearn == 0.0
Flask == 2.1.0
flask_cors == 3.0.10
cryptography == 36.0.1