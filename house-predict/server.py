import joblib
import pymysql
import numpy as np
from flask import Flask, request
from flask_cors import CORS
from datetime import datetime
from dateutil.relativedelta import relativedelta
import json


def data_process(house_res):
    conn = pymysql.connect(host='house-mysql.default.svc.cluster.local', user='root', passwd='house3421', database='house' , charset='utf8')
    cursor = conn.cursor()
    
    # 取出最早日期
    cursor.execute(f"SELECT value FROM normalization WHERE county = '{house_res['城市']}' AND item = '日期基準' ")
    first_dealAdDate = cursor.fetchone()[0]
    
    # 日期轉換
    house_dealAdDate_split = house_res['日期'].split('-')
    first_dealAdDate_split = first_dealAdDate.split('-')
    month = int(house_dealAdDate_split[0]) - int(first_dealAdDate_split[0])
    day = int(house_dealAdDate_split[1]) - int(first_dealAdDate_split[1])
    house_res['日期'] = 12 * month + day

    # 地區、類型LabelEncoder
    cursor.execute(f"SELECT value FROM normalization WHERE county = '{house_res['城市']}' AND type = '地區' AND item = '{house_res['地區']}' ")
    house_res['地區'] = cursor.fetchone()[0]
    cursor.execute(f"SELECT value FROM normalization WHERE county = '{house_res['城市']}' AND type = '類型' AND item = '{house_res['類型']}' ")
    house_res['類型'] = cursor.fetchone()[0]
    
    city = house_res['城市'] 
    house_res.pop('城市')
    house = []
    # 資料標準化
    for key,value in house_res.items():
        cursor.execute(f"SELECT value FROM normalization WHERE county = '{city}' AND type = '{key}' AND item = '平均數' ")
        ave = float(cursor.fetchone()[0])
        cursor.execute(f"SELECT value FROM normalization WHERE county = '{city}' AND type = '{key}' AND item = '標準差' ")
        std = float(cursor.fetchone()[0])
        if std != 0:
            house.append((float(value) - ave)/std)
        else:
            house.append(0)
    
    return house


def predict(city, house):
    model = joblib.load(f"house_model/{city}.pkl")
    price = int(model.predict(np.array([house]))[0])

    return price


app = Flask(__name__)
CORS(app)

# 預估當月價格
@app.route("/", methods=['GET'])
def index():
    try:
        # 參數
        city = request.args.get('city')
        district = request.args.get('district')
        regPin = request.args.get('regPin')
        landPin = request.args.get('landPin')
        age = request.args.get('age')
        caseType = request.args.get('caseType')
        mrtTags = request.args.get('mrtTags')
        carPrice = request.args.get('carPrice')

        # 現在日期
        dealAdDate = datetime.now().strftime('%Y-%m')
        house = {'日期':dealAdDate, '城市':city, '地區':district, '建坪':regPin, '地坪':landPin, '屋齡':age, '類型':caseType, '捷運':mrtTags, '車位':carPrice}
        
        # 轉成給模型的array
        house = data_process(house)

        # 模型預測
        price = predict(city, house)

        res = {'price':price, 'status':'ok'}
    except:
        res = {'status':'error'}

    finally:
        res = json.dumps(res)
        return res


# 預估一年內價格
@app.route("/month/", methods=['GET'])
def month():
    try:
    # 參數
        city = request.args.get('city')
        district = request.args.get('district')
        regPin = request.args.get('regPin')
        landPin = request.args.get('landPin')
        age = request.args.get('age')
        caseType = request.args.get('caseType')
        mrtTags = request.args.get('mrtTags')
        carPrice = request.args.get('carPrice')


        data = [['日期', '價格(萬元)']]

        for add_month in range(1, 8):
            dealAdDate = (datetime.now() + relativedelta(months = add_month)).strftime('%Y-%m')
            house = {'日期':dealAdDate, '城市':city, '地區':district, '建坪':regPin, '地坪':landPin, '屋齡':age, '類型':caseType, '捷運':mrtTags, '車位':carPrice}
            
            # 轉成給模型的array
            house = data_process(house)

            # 模型預測
            price = predict(city, house)
            
            dealAdDate = dealAdDate.split('-')[1]
            if dealAdDate[0] == '0':
                dealAdDate = dealAdDate.replace('0', '')

            data.append([dealAdDate + '月', price])

        res = {'price':data, 'status':'ok'}

    except:
        res = {'status':'error'}

    finally:
        res = json.dumps(res, ensure_ascii=False)
        return res

# 預估五年內價格
@app.route("/year/", methods=['GET'])
def year():
    try:
    # 參數
        city = request.args.get('city')
        district = request.args.get('district')
        regPin = request.args.get('regPin')
        landPin = request.args.get('landPin')
        age = request.args.get('age')
        caseType = request.args.get('caseType')
        mrtTags = request.args.get('mrtTags')
        carPrice = request.args.get('carPrice')


        data = [['日期', '價格(萬元)']]

        for add_month in range(1, 61, 12):
            dealAdDate = (datetime.now() + relativedelta(months = add_month)).strftime('%Y-%m')
            house = {'日期':dealAdDate, '城市':city, '地區':district, '建坪':regPin, '地坪':landPin, '屋齡':age, '類型':caseType, '捷運':mrtTags, '車位':carPrice}
            
            # 轉成給模型的array
            house = data_process(house)

            # 模型預測
            price = predict(city, house)

            data.append([dealAdDate.split('-')[0]+ '年', price])

        res = {'price':data, 'status':'ok'}

    except:
        res = {'status':'error'}

    finally:
        res = json.dumps(res, ensure_ascii=False)
        return res

if __name__ == '__main__':
    app.run(debug=True)

