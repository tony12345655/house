<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Basic;

class AreaController extends Controller
{
    public function index(Request $request){

        $city_arr = Basic::distinct() -> pluck('county');
        
        $data = [];
        foreach ($city_arr as $key => $value) {
            $area =  Basic::where([['county', '=', $value],['item', '=', 'district']]) -> pluck('value');
            $data[$key] = array('city' => $value, 'area' => $area);
        }

        return $data;
        
    }

}
