<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Basic;

class CasetypeController extends Controller
{
    public function index(Request $request){
        
        $city = $request -> input('city');
        $data = Basic::where([['county', '=', $city],['item', '=', 'caseType']]) -> pluck('value');

        return $data;
    }
}
