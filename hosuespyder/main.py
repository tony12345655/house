#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time : 2022/4/17 下午 10:20
# @Author : Aries
# @Site : 
# @File : main.py
# @Software: PyCharm


from house import Net_Price_Login

if __name__ == '__main__':
    city_arr = ['台北市', '新北市', '桃園市', '台中市', '台南市', '高雄市', '基隆市', '宜蘭縣', '新竹市', '新竹縣',
                '南投縣', '嘉義市', '嘉義縣', '雲林縣', '澎湖縣', '金門縣', '屏東縣', '台東縣', '花蓮縣', '連江縣']

    for city in city_arr:
        house = Net_Price_Login(city)
        data = house.GetData(start='2017-12',end='2015-01')
        house.SaveDB(data)
        print(city + '已存入資料庫')



