#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time : 2022/4/18 上午 12:03
# @Author : Aries
# @Site : 
# @File : house.py
# @Software: PyCharm

import requests
import pymysql


class Net_Price_Login:
    def __init__(self, city):
        self.city = city

    def GetData(self, start, end):
        # 建立日期格式
        def create_date(y, m):
            if len(str(m)) == 1:
                y_m = str(y) + '0' + str(m)
            else:
                y_m = str(y) + str(m)

            return y_m

        # 所有資料
        data = []
        # 請求的header
        header = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36'}
        try:
            # 取得日期
            year = int(start.split('-')[0])
            month = int(start.split('-')[1])
            year_month = create_date(year, month)
            try:
                while year_month >= end.replace('-',''):
                    page = 1
                    status = True
                    while status:
                        try:
                            res = requests.get(f'https://price.houseprice.tw/ws/list/{self.city}_city/{year_month}-{year_month}_date/?p={page}', headers=header)
                            res_json = res.json()
                            # 找不到資料時跳出
                            if res_json['message'] == "NoData":
                                status = False
                            dealCases_arr = res_json['priceInfo']['dealCases']
                            for item in dealCases_arr:
                                try:
                                    # 民國日期
                                    dealTwDate = item['dealTwDate']
                                    # 西園日期
                                    dealAdDate = item['adYYYY'] + '-' + item['adMM']
                                    # 城市
                                    county = item['county']
                                    # 地區
                                    district = item['district']
                                    # 地址
                                    address = item['address']
                                    # 樓層開始
                                    floorStart = item['floorStart']
                                    # 樓層結束
                                    floorEnd = item['floorEnd']
                                    # 總樓層
                                    upFloor = item['upFloor']
                                    # 屋齡
                                    age = item['age']
                                    # 捷運
                                    mrtTags = str(item['mrtTags'])
                                    # 建案
                                    communityTags = str(item['communityTags'])
                                    # 型態
                                    caseType = item['caseType']
                                    # 總價(萬)
                                    price = item['price']
                                    # 總價車位
                                    carPrice = item['carPrice']
                                    # 單價(萬)
                                    unitPrice = item['unitPrice']
                                    # 單價車位
                                    unitCarPrice = item['unitCarPrice']
                                    # 建坪
                                    regPin = item['regPin']
                                    # 地坪
                                    landPin = item['landPin']
                                    # 備註
                                    note = item['note']
                                    # 將資料存入串列
                                    data.append(
                                        {'dealTwDate': dealTwDate, 'dealAdDate': dealAdDate, 'county': county, 'district': district,
                                         'address': address, 'floorStart': floorStart,
                                         'floorEnd': floorEnd, 'upFloor': upFloor, 'age': age, 'mrtTags': mrtTags,
                                         'communityTags': communityTags, 'caseType': caseType, 'price': price,
                                         'carPrice': carPrice, 'unitPrice': unitPrice, 'unitCarPrice': unitCarPrice,
                                         'regPin': regPin, 'landPin': landPin, 'note': note})
                                except:
                                    pass
                        except:
                            pass
                        finally:
                            page += 1
                    # 變換日期區間
                    if month == 1:
                        year -= 1
                        month = 12
                    else:
                        month -= 1
                    year_month = create_date(year, month)
            except:
                pass
        except:
            pass

        return data

    @staticmethod
    def SaveDB(data):
        conn = pymysql.connect(host='localhost', user='root', passwd='1234', charset='utf8')
        cursor = conn.cursor()
        # 如果資料庫不存在建立資料庫
        db_exit = cursor.execute("SELECT * FROM information_schema.schemata WHERE schema_name = 'house' ")
        if db_exit == 0:
            cursor.execute("CREATE DATABASE house DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ")
            conn.commit()
        # 使用資料庫
        cursor.execute("use house")
        conn.commit()
        table_exit = cursor.execute("SHOW TABLES LIKE 'net_login' ")
        if table_exit == 0:
            cursor.execute("""
                       CREATE TABLE net_login(
                           Id INT NOT NULL AUTO_INCREMENT,
                           dealTwDate TEXT,
                           dealAdDate NVARCHAR(50),
                           county NVARCHAR(50),
                           district NVARCHAR(50),
                           address TEXT,
                           floorStart NVARCHAR(10),
                           floorEnd NVARCHAR(10),
                           upFloor NVARCHAR(10),
                           age NVARCHAR(10),
                           mrtTags TEXT,
                           communityTags TEXT,
                           caseType NVARCHAR(20),
                           price NVARCHAR(10),
                           carPrice NVARCHAR(10),
                           unitPrice NVARCHAR(10),
                           unitCarPrice NVARCHAR(10),
                           regPin NVARCHAR(10),
                           landPin NVARCHAR(10),
                           note TEXT,
                           PRIMARY KEY (Id)
                       )
                       """)
            conn.commit()
        # 將資料存入資料庫
        for item in data:
            try:
                sql = f"INSERT INTO net_login (dealTwDate, dealAdDate, county, district, address, floorStart, floorEnd, upFloor, age, mrtTags, communityTags, caseType, price, carPrice, unitPrice, unitCarPrice, regPin, landPin, note) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                one = (item['dealTwDate'], item['dealAdDate'], item['county'], item['district'], item['address'], item['floorStart'], item['floorEnd'], item['upFloor'], item['age'], item['mrtTags'], item['communityTags'], item['caseType'], item['price'], item['carPrice'], item['unitPrice'], item['unitCarPrice'], item['regPin'], item['landPin'], item['note'])
                cursor.execute(sql, one)
                conn.commit()
            except:
                pass

        conn.close()
